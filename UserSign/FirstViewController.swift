//
//  FirstViewController.swift
//  UserSign
//
//  Created by Yıldırımhan Atçıoğlu on 1.12.2017.
//  Copyright © 2017 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    @IBOutlet weak var mailField: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    @IBAction func loginButton(_ sender: Any) {
        loginUser(urlString: "http://localhost:8888/login.php")
    }
    
    func loginUser(urlString:String) {
        
            let urlRequest = URL(string: urlString)
            var request = URLRequest(url:urlRequest! as URL)
            request.httpMethod = "POST"
        
            let mail = mailField.text
            let password = passwordField.text
        
            
            let parameters = "mail="+mail!+"&password="+password!
            request.httpBody = parameters.data(using: String.Encoding.utf8)
            
            
            
        
        
            if ((mail?.isEmpty)! || (password?.isEmpty)!){
                print("Hata eksik bilgi girdiniz")
            }
            
            let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
                if error != nil
                {
                    print(error!)
                }
                else{
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                        if let jsonParse = json
                        { var message:String!
                            var status:String!
                            
                            message = jsonParse["mesaj"] as! String?
                            status = jsonParse["durum"] as! String?
                            if (status == "basarili"){
                                print(message)
                               
                                DispatchQueue.main.sync {
                                    let storyBoard:UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
                                    let homeVC = storyBoard.instantiateViewController(withIdentifier:"HomePage") as! HomeViewController
                                    self.present(homeVC, animated:true,completion: nil)
                                    
                                    self.mailField.text = ""
                                    self.passwordField.text = ""
                                   
                                }
                                
                                
                                
                            }
                            else{
                                print(message)
                                
                            }
                            
                            
                            
                        }
                    }
                    catch{
                        print(error)
                    }
                    
                }
            }
            task.resume()
            
       
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

