//
//  SecondViewController.swift
//  UserSign
//
//  Created by Yıldırımhan Atçıoğlu on 1.12.2017.
//  Copyright © 2017 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordAgainField: UITextField!
    @IBAction func saveButton(_ sender: Any) {
        registerUser(urlString: "http://localhost:8888/kaydet.php")
        
    }
    
    func registerUser(urlString:String){
        
       
        let urlRequest = URL(string: urlString)
        var request = URLRequest(url:urlRequest! as URL)
        request.httpMethod = "POST"
        
        let name = nameField.text
        let mail = mailField.text
        let password = passwordField.text
        let password2 = passwordAgainField.text
        
        let parameters = "name="+name!+"&mail="+mail!+"&password="+password!
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        
        
        print("isim: \(name!), soyisim: \(mail!), şifre: \(password!),şifre2: \(password2!)")
        if password != password2{
            print("şifreler birbiriyle uyuşmadı!")
        }
        if ((name?.isEmpty)! || (mail?.isEmpty)! || (password?.isEmpty)! || (password2?.isEmpty)!){
            print("Hata eksik bilgi girdiniz")
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) in
            if error != nil
            {
                print(error!)
            }
            else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSDictionary
                  if let jsonParse = json
                  { var message:String!
                    var status:String!
                    
                    message = jsonParse["mesaj"] as! String?
                    status = jsonParse["durum"] as! String?
                    if (status == "basarili"){
                        print(message)
                        DispatchQueue.main.sync {
                            self.nameField.text = ""
                            self.mailField.text = ""
                            self.passwordField.text = ""
                            self.passwordAgainField.text = ""
                        }
                       
                        
                       
                        }
                    else{
                        print(message)
                        
                    }
                    
                  
                    
                    }
                }
                catch{
                    print(error)
                }
                
            }
        }
        task.resume()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

